package com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class MyFriendViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public MyFriendViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MyFriendViewModel(mApplication);
    }
}
