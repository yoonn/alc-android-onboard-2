package com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.repositories.MyFriendRepository;

import java.util.List;

public class MyFriendViewModel extends AndroidViewModel {

    private MyFriendRepository mMyFriendRepository;

    public MyFriendViewModel(@NonNull Application application) {
        super(application);

        mMyFriendRepository = MyFriendRepository.getInstance();
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return mMyFriendRepository.getMyFriendAdapterListLiveData();
    }

    public void loadMyFriendAdapterList() {
        mMyFriendRepository.loadMyFriendAdapterList(getApplication());
    }

    public void addFriend(String lastName, String FirstName, String email, String avatar) {
        mMyFriendRepository.addFriend(getApplication(), lastName, FirstName, email, avatar);
    }

    public void editFriend(Long id, String lastName, String FirstName, String email) {
        mMyFriendRepository.editFriend(getApplication(), id, lastName, FirstName, email);
    }

    public void deleteFriend(Long id) {
        mMyFriendRepository.deleteFriend(getApplication(), id);
    }
}
