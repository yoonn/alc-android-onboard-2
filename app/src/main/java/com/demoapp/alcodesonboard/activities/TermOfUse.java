package com.demoapp.alcodesonboard.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.LoginFragment;
import com.demoapp.alcodesonboard.fragments.TermOfUseFragment;

import butterknife.ButterKnife;

public class TermOfUse extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_term_of_use);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(TermOfUseFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, TermOfUseFragment.newInstance(), TermOfUseFragment.TAG)
                    .commit();
        }
    }
}
