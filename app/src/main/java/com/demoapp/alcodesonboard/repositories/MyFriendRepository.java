package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.database.entities.MyFriendDao;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class MyFriendRepository {

    private static MyFriendRepository mInstance;

    private MutableLiveData<List<MyFriendAdapter.DataHolder>> mMyFriendAdapterListLiveData = new MutableLiveData<>();

    public static MyFriendRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendRepository.class) {
                mInstance = new MyFriendRepository();
            }
        }

        return mInstance;
    }

    private MyFriendRepository() {
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return mMyFriendAdapterListLiveData;
    }

    public void loadMyFriendAdapterList(Context context) {
        List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if (records != null) {
            for (MyFriend myFriend : records) {
                MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
                dataHolder.id = myFriend.getId();
                dataHolder.lastName = myFriend.getLastName();
                dataHolder.FirstName = myFriend.getFirstName();
                dataHolder.email = myFriend.getEmail();
                dataHolder.avatar = myFriend.getAvatar();



                dataHolders.add(dataHolder);
            }
        }

        mMyFriendAdapterListLiveData.setValue(dataHolders);
    }

    public void addFriend(Context context, String lastName, String FirstName, String email, String avatar) {
        // Create new record.
        MyFriend myFriend = new MyFriend();
        myFriend.setLastName(lastName);
        myFriend.setFirstName(FirstName);
        myFriend.setEmail(email);
        myFriend.setAvatar(avatar);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .insert(myFriend);

        // Done adding record, now re-load list.
        loadMyFriendAdapterList(context);
    }

    public void editFriend(Context context, Long id, String lastName, String FirstName, String email) {
        MyFriendDao myFriendDao = DatabaseHelper.getInstance(context).getMyFriendDao();
        MyFriend myFriend = myFriendDao.load(id);

        // Check if record exists.
        if (myFriend != null) {
            // Record is found, now update.
            myFriend.setLastName(lastName);
            myFriend.setFirstName(FirstName);
            myFriend.setEmail(email);

            myFriendDao.update(myFriend);

            // Done editing record, now re-load list.
            loadMyFriendAdapterList(context);
        }
    }

    public void deleteFriend(Context context, Long id) {
        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .deleteByKey(id);

        // Done deleting record, now re-load list.
        loadMyFriendAdapterList(context);
    }
}

