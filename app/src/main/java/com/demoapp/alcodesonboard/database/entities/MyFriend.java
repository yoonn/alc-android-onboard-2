package com.demoapp.alcodesonboard.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class MyFriend {

    @Id(autoincrement = true)
    private Long id;

    @NotNull
    private String lastName;

    @NotNull
    private String FirstName;

    @NotNull
    private String email;

    @NotNull
    private String avatar;

    @Generated(hash = 1001532608)
    public MyFriend(Long id, @NotNull String lastName, @NotNull String FirstName,
            @NotNull String email, @NotNull String avatar) {
        this.id = id;
        this.lastName = lastName;
        this.FirstName = FirstName;
        this.email = email;
        this.avatar = avatar;
    }

    @Generated(hash = 15986203)
    public MyFriend() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
