package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.FriendListActivity;
import com.demoapp.alcodesonboard.activities.TermOfUse;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();



    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_password)
    protected TextInputEditText mEditTextPasword;

    @BindView(R.id.button_login)
    protected MaterialButton mButtonLogin;

    @BindView(R.id.textView)
    protected  TextView mTextView;

    private Unbinder mUnbinder;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPasword.setText("cityslicka");
        }


        mTextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v){
                Intent intent=new Intent(getActivity(),TermOfUse.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @OnClick(R.id.button_login)
    protected void doLogin() {
        // Disable button and wait for server response.
        mButtonLogin.setEnabled(false);
        mButtonLogin.setText("Loading");

        // TODO change login button's label to "Loading"
        // TODO remember reset label to "Login" when needed.

        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPasword.getText().toString(); // Password should not trim.

        if (email.isEmpty() || password.isEmpty())
        {
            Toast.makeText(getActivity(), "No Blank allowed", Toast.LENGTH_SHORT).show();
            mButtonLogin.setEnabled(true);
            mButtonLogin.setText("Login");
            return;
        }

        // TODO check email and password is blank or not.
        // TODO show error message and stop process if validation failed.

        // Call login API.
        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();

                // TODO bad practice, should check user is still staying in this app or not after server response.

                // Convert JSON string to Java object.
                LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                // Save user's email to shared preference.
                SharedPreferenceHelper.getInstance(getActivity())
                        .edit()
                        .putString("email", email)
                        .putString("token", responseModel.token)
                        .apply();

                startActivity(new Intent(getActivity(), FriendListActivity.class));
                getActivity().finish();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("d;;Login error: %s", error.getMessage());

                // TODO bad practice, should check user is still staying in this app or not after server response.
                if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                    // Show error in popup dialog.
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content("Invalid email or password.")
                            .positiveText("OK")
                            .show();
                }
                mButtonLogin.setEnabled(true);
                mButtonLogin.setText("Login");
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
    }
}
