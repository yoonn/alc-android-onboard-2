package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.demoapp.alcodesonboard.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TermOfUseFragment extends Fragment {

    public static final String TAG = TermOfUseFragment.class.getSimpleName();

    @BindView(R.id.textviewtou)
    protected TextView mTextView;

    @BindView((R.id.SCROLLER_ID))
    protected ScrollView mScrollView;

    private Unbinder mUnbinder;

    public TermOfUseFragment() {
    }

    public static TermOfUseFragment newInstance() {
        return new TermOfUseFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_term_of_use, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    private void scrollToBottom()
    {
        mScrollView.post(new Runnable()
        {
            public void run()
            {
                mScrollView.smoothScrollTo(0, mTextView.getBottom());
            }
        });
    }
}
