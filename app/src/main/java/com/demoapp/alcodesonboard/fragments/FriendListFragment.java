package com.demoapp.alcodesonboard.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.FriendDetailsActivity;
import com.demoapp.alcodesonboard.activities.LoginActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyFriendViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyFriendViewModelFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FriendListFragment extends Fragment implements MyFriendAdapter.Callbacks{

    public static final String TAG = FriendListFragment.class.getSimpleName();

    @BindView(R.id.recyclerview1)
    protected RecyclerView recyclerView1;

    @BindView(R.id.textviewNoFriend)
    protected TextView Textview1;

    @BindView(R.id.button_friend)
    protected Button btnFriend;

    private final int REQUEST_CODE_MY_FRIEND_DETAIL = 300;

    private Unbinder mUnbinder;
    private MyFriendAdapter mAdapter;
    private MyFriendViewModel mViewModel;

    public FriendListFragment() {
    }

    public static FriendListFragment newInstance() {
        return new FriendListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);

            setHasOptionsMenu(true);


        }

        @OnClick(R.id.button_friend)

        protected void getFriend(){


            String url = "https://reqres.in/api/users?page=2";

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String lastName = jsonObject.getString("last_name");
                            String FirstName = jsonObject.getString("first_name");
                            String email = jsonObject.getString("email");
                            String avatar = jsonObject.getString("avatar");

                            mViewModel = new ViewModelProvider(getActivity(), new MyFriendViewModelFactory(getActivity().getApplication())).get(MyFriendViewModel.class);

                            mViewModel.addFriend(FirstName, lastName, email, avatar);
                            btnFriend.setVisibility(View.GONE);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Textview1.setVisibility(View.VISIBLE);
                    error.printStackTrace();
                }
            });

            requestQueue.add(jsonObjectRequest);
        }

//        public void loadImage(View view){
//            Glide.with(getActivity())
//                    .load(avatar)
//                    .into(imgFriend);
//        }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_main, menu);
    }

//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        int itemId = item.getItemId();
//
//        if (itemId == R.id.menu_add) {
//            startActivityForResult(new Intent(getActivity(), FriendDetailsActivity.class), REQUEST_CODE_MY_FRIEND_DETAIL);
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_logout) {
            // TODO show confirm dialog before continue.


            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure you want to Exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();

                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                        }
                    })
                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();


            // Logout user.
            // Clear all user's data.

        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_MY_FRIEND_DETAIL && resultCode == FriendDetailsActivity.RESULT_CONTENT_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadMyFriendAdapterList();
        }
    }

    @Override
    public void onListItemClicked(MyFriendAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), FriendDetailsActivity.class);
        intent.putExtra(FriendDetailsActivity.EXTRA_LONG_MY_NOTE_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_DETAIL);
    }

    @Override
    public void onDeleteButtonClicked(MyFriendAdapter.DataHolder data) {
        mViewModel.deleteFriend(data.id);
    }

    private void initView() {
        mAdapter = new MyFriendAdapter(getActivity());
        mAdapter.setCallbacks(this);

        recyclerView1.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setAdapter(mAdapter);
    }

    private void initViewModel() {
//        mViewModel = ViewModelProviders.of(this).get(MyFriendViewModel.class);
        mViewModel = new ViewModelProvider(this, new MyFriendViewModelFactory(getActivity().getApplication())).get(MyFriendViewModel.class);
        mViewModel.getMyFriendAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyFriendAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                if(dataHolders.isEmpty())
                {
                    btnFriend.setVisibility(View.VISIBLE);
                    Textview1.setVisibility(View.VISIBLE);
                }else
                {
                    Textview1.setVisibility(View.GONE);
                    btnFriend.setVisibility(View.GONE);
                }
                // TODO check dataHolders has data or not.
                // TODO show list if have, otherwise show label: "No data"
            }
        });

        // Load data into adapter.
        mViewModel.loadMyFriendAdapterList();
    }
}


