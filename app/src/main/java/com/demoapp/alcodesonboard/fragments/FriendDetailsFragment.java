package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.FriendListActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyFriendViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyFriendViewModelFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendDetailsFragment extends Fragment {

    public static final String TAG = FriendDetailsFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.textViewFirstName)
    protected TextView mTextViewFirstName;

    @BindView(R.id.textViewLastName)
    protected TextView mTextViewLastName;

    @BindView(R.id.textViewEmail)
    protected TextView mTextViewEmail;

    @BindView(R.id.imgFriend2)
    protected ImageView mImg;

    private Unbinder mUnbinder;
    private Long mMyFriendId = 0L;
    private MyFriendViewModel mViewModel;





    public FriendDetailsFragment() {
    }

    public static FriendDetailsFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        FriendDetailsFragment fragment = new FriendDetailsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_details, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyFriendId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note.
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue.
            startActivity(new Intent(getActivity(), FriendListActivity.class));

            getActivity().finish();
//            String lastName = mTextViewLastName.getText().toString();
//            String FirstName = mTextViewFirstName.getText().toString();
//            String email = mTextViewEmail.getText().toString();
//            if (FirstName.isEmpty() || lastName.isEmpty())
//            {
//                Toast.makeText(getActivity(), "No Blank allowed", Toast.LENGTH_SHORT).show();
//                return false;
//            }
//
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setMessage("Are you sure you want to Save?")
//                    .setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//
//
//                            MyFriend myFriend = new MyFriend();
//                            myFriend.setLastName(lastName);
//                            myFriend.setFirstName(FirstName);
//                            myFriend.setEmail(email);
//
//                            // TODO BAD practice, should move Database operations to Repository.
//                            if (mMyFriendId > 0) {
//                                // Update record.
//                                myFriend.setId(mMyFriendId);
//
////                                DatabaseHelper.getInstance(getActivity())
////                                        .getMyNoteDao()
////                                        .save(myNote);
//                                mViewModel.editFriend(mMyFriendId, lastName, FirstName, email);
//                            } else {
//                                // Create record.
////                                DatabaseHelper.getInstance(getActivity())
////                                        .getMyNoteDao()
////                                        .insert(myNote);
//                                mViewModel.addFriend(lastName, FirstName,  email);
//                            }
//
//                            Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();
//
//                            getActivity().setResult(FriendDetailsActivity.RESULT_CONTENT_MODIFIED);
//                            getActivity().finish();
//
//
//                        }
//                    })
//                    .setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    });
//            AlertDialog alertDialog = builder.create();
//            alertDialog.show();
//            // TODO check email and password is blank or not.
//
//
//            // Save record and return to list.

        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository.
        if (mMyFriendId > 0) {
            MyFriend myFriend = DatabaseHelper.getInstance(getActivity())
                    .getMyFriendDao()
                    .load(mMyFriendId);

            if (myFriend != null) {

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_launcher_foreground);

                Glide.with(this)
                        .load(myFriend.getAvatar())
                        .apply(requestOptions)
                        .into(mImg);
                mTextViewLastName.setText(myFriend.getLastName());
                mTextViewFirstName.setText(myFriend.getFirstName());
                mTextViewEmail.setText(myFriend.getEmail());
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }

    private void initViewModel(){
        mViewModel = new ViewModelProvider(this, new MyFriendViewModelFactory(getActivity().getApplication())).get(MyFriendViewModel.class);

        mViewModel.loadMyFriendAdapterList();
    }


}
