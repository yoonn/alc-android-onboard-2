package com.demoapp.alcodesonboard.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.demoapp.alcodesonboard.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    private static Activity mFriend;

    public MyFriendAdapter(Activity activity){
        mFriend=activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent, false));
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder{

        public Long id;
        public String FirstName;
        public String lastName;
        public String email;
        public String avatar;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.textview_friend_list)
        public TextView friend;

        @BindView(R.id.textview_friend_list1)
        public TextView friend1;

        @BindView(R.id.textview_friend_list2)
        public TextView friend2;

        @BindView(R.id.imageFriend)
        public ImageView mImgFriend;


//        @BindView(R.id.button_delete)
//        public MaterialButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_launcher_foreground);

                Glide.with(mFriend)
                        .load(data.avatar)
                        .apply(requestOptions)
                        .into(mImgFriend);

                friend.setText(data.FirstName);
                friend1.setText(data.lastName);
                friend2.setText(data.email);



                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });

//                    deleteButton.setOnClickListener(new View.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            callbacks.onDeleteButtonClicked(data);
//                        }
//                    });
                }
            }
        }

        public void resetViews() {
            friend.setText("");
            root.setOnClickListener(null);
//            deleteButton.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);
    }
}


